#include <QCoreApplication>
#include <QObject>
#include <QString>
#include <QTimer>
#include <QDebug>

int main(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);

  qDebug() << "conan-qt-builds test";

  QTimer::singleShot(0, &app, &QCoreApplication::quit);

  return app.exec();
}
