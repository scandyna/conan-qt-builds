from conans import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps
import os

class ConanQtTest(ConanFile):
  name = "conan-qt-test"
  settings = "os", "compiler", "build_type", "arch"

  def build_requirements(self):
    self.tool_requires("MdtCMakeModules/0.18.3@scandyna/testing", force_host_context=True)

  def generate(self):

    tc = CMakeToolchain(self)
    if self.settings.os == "Linux":
      if self.settings.compiler.sanitizer == "Thread":
        tc.variables["SANITIZER_ENABLE_THREAD"] = "ON"
    tc.generate()

    deps = CMakeDeps(self)
    deps.generate()

  def build(self):
      cmake = CMake(self)
      cmake.configure()
      cmake.build()

  def test(self):
      cmake = CMake(self)
      cmake.test()
