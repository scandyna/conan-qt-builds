# conan-qt-builds

Build Some Qt Conan binaries

Qt Conan packages are provided by [ConanCenter](https://conan.io/center).
The recipes are avaliable on [GitHub](https://github.com/conan-io/conan-center-index).

Because the combination of binaries is big,
some personal builds are made from here,
avoiding to rebuiuld Qt every time.

# Usage

## Add required remotes

Add the conan package registry where the binaries will be available:
```bash
conan remote add gitlab https://gitlab.com/api/v4/projects/25668674/packages/conan
```

## Install profiles

The various builds are guided by Conan profiles.

See https://gitlab.com/scandyna/conan-config

# Install packages

Create a build directory and cd to it:
```bash
mkdir build && cd build
```

Example to install Qt5 core:
```bash
conan install qt/5.15.14@ --profile:build linux_gcc13_x86_64 --profile:host linux_ubuntu-24.04_gcc13_x86_64_qt_and_more --settings:build build_type=Release --settings:host build_type=Release --options:host qt:shared=True --build missing --update
```

## Install packages with sanitizers support

To model the fact that the binaries compiled with sanitizers support
are different, it should be described as a compiler sub-setting.
See the [Conan compiler sanitizers](https://docs.conan.io/en/1.19/howtos/sanitizers.html)
for a explanation.

The `settings.yml` and profiles used in my projects are available in my
[conan-config](https://gitlab.com/scandyna/conan-config) repository.

Example to install a Qt package with thread sanitizer:
```bash
conan install qt/5.14.0@bincrafters/stable --profile linux_clang6.0_x86_64_libc++_tsan_qt_core --build missing --remote scandyna
```


# OLD stuff

## Install original Bincrafters packages

To install a Qt package with default options and profile:
```bash
conan install qt/5.x.y@bincrafters/stable
```

Notice that the packages recipes are created by the Bincrafters,
but some binaries are available on the scandyna bintray.

## Install packages from the Scandyna fork

Some options that Qt provides are not (yet) in the Bincrafters conan recipes.
A typical case is compile Qt itself with [sanitizers](https://github.com/google/sanitizers).

To install a Qt package with default options and profile:
```bash
conan install qt/5.x.y@scandyna/testing
```

## Install packages with sanitizers support

To model the fact that the binaries compiled with sanitizers support
are different, it should be described as a compiler sub-setting.
See the [Conan compiler sanitizers](https://docs.conan.io/en/1.19/howtos/sanitizers.html)
for a explanation.

The `settings.yml` and profiles used in my projects are available in my
[conan-config](https://gitlab.com/scandyna/conan-config) repository.

To install a Qt package with default options and profile:
```bash
conan install qt/5.x.y@scandyna/testing -pr linux_gcc8_x86_64_tsan -o qt:sanitize_thread=True --build missing
```

# Build packages from the Scandyna fork

Create a build directory and cd to it:
```bash
mkdir build && cd build
```

Get the scandyna fork from [GitHub](https://github.com/scandyna/conan-qt):
```bash
git clone https://github.com/scandyna/conan-qt.git
```

Switch to the branch matching the targeted Qt version:
```bash
cd conan-qt
git checkout testing/5.x.y-xsan
cd ..
```

Copy the recipe to your local cache:
```bash
conan export conan-qt qt/5.x.y@scandyna/testing
```

To install a Qt package with default options and profile:
```bash
conan install qt/5.x.y@scandyna/testing --build missing
```

To install a Qt package with a configuration file and specify options, a profile and settings:
```bash
conan install ../qt-5.14.0-core/ -pr linux_gcc8_x86_64_tsan -o qt:sanitize_thread=True --build missing
```


Using the native profile:
```bash
conan install ../qt-5.13.2-core --build missing
```

Using a specific profile and also build type:
```bash
conan install ../qt-5.13.2-core --build missing -pr linux_gcc7_x86_64 -s build_type=RelWithDebInfo
```

Some profiles can require custom Conan settings.
See [scandyna conan config](https://gitlab.com/scandyna/conan-config)
for more informations.

# Upload packages

```bash
conan upload qt/5.13.2@bincrafters/stable -r scandyna --all
```
